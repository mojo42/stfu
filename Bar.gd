# STFU - Alert when your mic records too much noise
# Copyright (C) 2021  Jérôme Jutteau <jerome@jutteau.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node2D

var _red_zone = 101
var _ok_color = Color("4d8941")
var _ko_color = Color("ee0000")
var _length

func _ready():
	_length = $Background.size.x
	red_zone(0.5)
	$Background.size.x = _length

func level(lvl: float):
	var new_size = min(_length, int(lvl * _length))
	$Level.size = Vector2(new_size, $Level.size.y)
	if lvl > _red_zone:
		$Level.color = _ko_color
	else:
		$Level.color = _ok_color

func red_zone(lvl: float):
	_red_zone = lvl
	var red_pos = min(_length, int(lvl * _length))
	$RedZone.position = Vector2(red_pos, $RedZone.position.y)
