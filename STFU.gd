# STFU - Alert when your mic records too much noise
# Copyright (C) 2021  Jérôme Jutteau <jerome@jutteau.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Control

const version_major = 0
const version_minor = 4
const version_patch = 0
const Buffer = preload("res://Buffer.gd")
const sensibility_multiplicator = 10
var record_buffer
var measure_buffer
var level = 0.0
var level_exceeded = false
var exceeded_counter = 0
var record_duration_s = 3
var sensibility_percent = 20

func _ready():
	_init_record_dir()
	record_buffer = Buffer.new("Record", 0, int(record_duration_s))
	record_buffer.start()
	measure_buffer = Buffer.new("Record", 1, 1)
	measure_buffer.start()
	update_record_duration_s(record_duration_s)
	$Sensibility/Slider.value = sensibility_percent
	set_sensibility_slider_color(sensibility_percent)
	_load_config()
	$STFULink.text += version()
	$Mic/Bar.red_zone($Threshold/Slider.value / 100.0)
	record_buffer.set_duration(int(record_duration_s))
	
func _init_record_dir():
	DirAccess.make_dir_absolute("user://records")

func version() -> String :
	return "v" + str(version_major) + "." + str(version_minor) + "." + str(version_patch)


func _on_MicUpdater_timeout():
	var has_exceeded = _update_level_status()
	$Mic/Bar.level(level)
	if has_exceeded:
		exceeded_counter += 1
		$Counter/Value.text = str(exceeded_counter)
		$Counter/Value.add_theme_color_override("font_color", Color(1, 0, 0, 1))
		if $Noise/Enabled.button_pressed:
			$Noise/Player.play()
		if $Record/Enabled.button_pressed:
			$Record/TimerSaveToFile.wait_time = record_duration_s / 2.0
			$Record/TimerSaveToFile.start()

func _update_level_status() -> bool:
	var sound_level_multiplicator = exp(-(sensibility_multiplicator / 2) + sensibility_percent * sensibility_multiplicator / 100)
	print(sound_level_multiplicator)
	level = measure_buffer.sound_level() * sound_level_multiplicator
	var limit = $Threshold/Slider.value / 100.0
	var has_exceeded = false
	if level > limit:
		if level_exceeded == false:
			has_exceeded = true
		level_exceeded = true
		$LevelExceededCoolDown.stop()
		$LevelExceededCoolDown.start()
	return has_exceeded

func _on_LevelExceededCoolDown_timeout():
	level_exceeded = false
	$Counter/Value.add_theme_color_override("font_color", Color(1, 1, 1, 1))

func _save_buffer():
	var d = Time.get_datetime_dict_from_system()
	var date = str(d.get("year")) + "-" + str(d.get("month")) + "-" + str(d.get("day")) + " " + str(d.get("hour")) + "h" + str(d.get("minute")) + "m" + str(d.get("second")) + "s"
	record_buffer.audio().save_to_wav("user://records/" + date + ".wav")

func _on_STFULink_pressed():
	var _err = OS.shell_open("https://gitlab.com/mojo42/stfu")

# https://docs.godotengine.org/fr/stable/tutorials/io/data_paths.html
func _on_RecordLink_pressed():
	var _err = OS.shell_open(ProjectSettings.globalize_path("user://records"))

func _on_ThresholdSlider_value_changed(value):
	$Mic/Bar.red_zone(value / 100.0)
	_save_config()

func _on_RecordSeconds_value_changed(value):
	record_buffer.set_duration(value)
	_save_config()

func _save_config():
	var data = {
		"version_major": version_major,
		"threshold": int($Threshold/Slider.value),
		"noise_active": $Noise/Enabled.button_pressed,
		"record_active": $Record/Enabled.button_pressed,
		"record_duration_s": record_duration_s,
		"sensibility_percent": sensibility_percent,
	}
	var config_file = FileAccess.open("user://config.json", FileAccess.WRITE)
	var data_string = JSON.stringify(data)
	config_file.store_line(data_string)
	config_file.close()

func _load_config():
	if not FileAccess.file_exists("user://config.json"):
		return
	var config_file = FileAccess.open("user://config.json", FileAccess.READ)
	var test_json_conv = JSON.new()
	test_json_conv.parse(config_file.get_line())
	var data = test_json_conv.get_data()
	if typeof(data) != TYPE_DICTIONARY:
		return
	var _version_major = data.get("version_major")
	if typeof(_version_major) != TYPE_FLOAT || int(_version_major) > version_major:
		return
	var threshold = data.get("threshold")
	if typeof(threshold) == TYPE_FLOAT:
		$Threshold/Slider.value = int(data.get("threshold"))
	var noise_active = data.get("noise_active")
	if typeof(noise_active) == TYPE_BOOL:
		$Noise/Enabled.button_pressed = noise_active
	var record_active = data.get("record_active")
	if typeof(record_active) == TYPE_BOOL:
		$Record/Enabled.button_pressed = record_active
	var data_record_duration_s = data.get("record_duration_s")
	if typeof(data_record_duration_s) == TYPE_FLOAT or typeof(data_record_duration_s) == TYPE_INT:
		update_record_duration_s(data_record_duration_s)
	var sensibility = data.get("sensibility_percent")
	if typeof(sensibility) == TYPE_FLOAT or typeof(sensibility) == TYPE_INT:
		sensibility_percent = sensibility
		$Sensibility/Slider.value = sensibility
		set_sensibility_slider_color(sensibility)

func update_record_duration_s(duration):
	record_duration_s = int(duration)
	$Record/Label.text = "Record %s seconds" % str(record_duration_s)

func _on_NoiseEnabled_toggled(_button_pressed):
	_save_config()

func _on_RecordEnabled_toggled(_button_pressed):
	_save_config()

func _on_RecordTimerSaveToFile_timeout():
	_save_buffer()

func _on_CounterReset_pressed():
	exceeded_counter = 0
	$Counter/Value.text = "0"

func _on_MicRecordUpdateTimer_timeout():
	record_buffer._update()

func _on_plus_pressed():
	update_record_duration_s(min(1000, record_duration_s + 1))
	_save_config()

func _on_minus_pressed():
	update_record_duration_s(max(1, record_duration_s - 1))
	_save_config()

func set_sensibility_slider_color(value):
	var green_and_blue = abs(100.0 - value) / 100.0
	$Sensibility/Slider.set_modulate(Color(1.0, green_and_blue, green_and_blue, 1.0))

func _on_slider_value_changed(value):
	sensibility_percent = value
	set_sensibility_slider_color(value)
	_save_config()
