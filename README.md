# STFU

STFU is a small application to alert you when your mic records too much noise.

![STFU screenshot](screenshot-v0.4.0.png)

Helpful? You can [buy me a coffee](https://www.paypal.com/paypalme/jeromejutteau).

# Features

- Show mic level
- Setup threshold
- (optional) trigger alert sound
- (optional) record to file when triggered

# Download & Installation

[Download](https://gitlab.com/mojo42/stfu/-/releases) the latest STFU version.

# Contributing

- Install [Godot Engine](https://godotengine.org/) and open project.
- For Windows packaging, install [Inno Setup](https://jrsoftware.org/isinfo.php)

# Licenses

- STFU code is licensed under [GPL-3.0-or-later](https://www.gnu.org/licenses/gpl-3.0-standalone.html).
- `error_008.wav` is created/distributed by [Kenney](https://www.kenney.nl) and licensed under [CC0-1.0](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

