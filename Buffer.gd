# STFU - Alert when your mic records too much noise
# Copyright (C) 2021  Jérôme Jutteau <jerome@jutteau.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node

class_name Buffer

var buffer
var max_duration_s
var audio_effect

func _init(device: String, bus_effect: int, dur_s: int = 3):
	var idx = AudioServer.get_bus_index(device)
	audio_effect = AudioServer.get_bus_effect(idx, bus_effect)
	max_duration_s = dur_s
	
func set_duration(dur_s):
	max_duration_s = dur_s
	
func _reset():
	audio_effect.set_recording_active(false)
	buffer = AudioStreamWAV.new()
	buffer.stereo = true
	buffer.format = audio_effect.format

func is_recording() -> bool:
	return audio_effect.is_recording_active()

func start():
	_reset()
	audio_effect.set_recording_active(true)

func stop():
	_update()
	audio_effect.set_recording_active(false)

func duration_s() -> float:
	return buffer.data.size() / _bytes_per_sample() / buffer.mix_rate

func _update():
	if not is_recording() || buffer == null:
		return

	var new_record = audio_effect.get_recording()
	if new_record == null || new_record.data == null || new_record.data.size() == 0:
		return

	var new_data = PackedByteArray()
	if buffer.data.size() > 0:
		new_data.append_array(buffer.data)
	if new_record.data.size() > 0:
		new_data.append_array(new_record.data)
	if new_data.size() == 0:
		return
	var max_buffer_size = int(buffer.mix_rate * _bytes_per_sample() * max_duration_s)
	var data_start = max(0, new_data.size() - max_buffer_size)
	buffer.data = new_data.slice(data_start)

	audio_effect.set_recording_active(false)
	audio_effect.set_recording_active(true)

func _bytes_per_sample() -> float:
	var bytes = 1
	if buffer.format == AudioStreamWAV.FORMAT_8_BITS:
		bytes *= 1
	elif buffer.format == AudioStreamWAV.FORMAT_16_BITS:
		bytes *= 2
	elif buffer.format == AudioStreamWAV.FORMAT_IMA_ADPCM:
		bytes *= 4
	if buffer.stereo:
		bytes *= 2
	return bytes

func audio() -> AudioStreamWAV:
	_update()
	return buffer

func debug_save_audio():
	var file = FileAccess.open("user://buffer.csv", FileAccess.WRITE)
	for i in range(min(buffer.data.size(), 1000)):
		file.store_string(str(int(_byte_to_float(buffer.data[i]) * 100)) + ";")
	file.close()

func sound_level() -> float:
	_update()
	return _mean_sound_level_8bits()

func _max_sound_level_8bits() -> float:
	var sampled_size = 100
	if buffer.format != AudioStreamWAV.FORMAT_8_BITS:
		return 0.0
	var buffer_size = buffer.data.size()
	if buffer_size < sampled_size:
		return 0.0
	var maximum = 0.0
	for i in range(buffer_size - sampled_size, buffer_size - 1):
		var absoluted = abs(_byte_to_float(buffer.data[i]))
		if absoluted > maximum:
			maximum = absoluted
	return maximum

func _mean_sound_level_8bits() -> float:
	var sampled_size = 1000
	if buffer.format != AudioStreamWAV.FORMAT_8_BITS:
		return 0.0
	var buffer_size = buffer.data.size()
	if buffer_size < sampled_size:
		return 0.0
	var mean = 0.0
	for i in range(buffer_size - sampled_size, buffer_size - 1):
		mean += abs(_byte_to_float(buffer.data[i]))
	mean /= sampled_size
	return mean

# Thanks for convertion tip on 
# https://godotengine.org/qa/67091/how-to-read-audio-samples-as-1-1-floats
func _byte_to_float(byte):
		var converted = (byte + 128) & 0xff
		return float(converted - 128) / 128.0
